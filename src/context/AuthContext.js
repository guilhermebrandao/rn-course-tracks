import { AsyncStorage } from 'react-native'
import createDataContext from './createDataContext';
import trackApi from '../api/tracker'
import { navigate } from '../navigationRef';

const authReducer = (state, action) => {
  switch (action.type) {
    case 'add_error':
      return { ...state, errorMessage: action.payload };
    case 'signin':
      return { errorMessage: '', token: action.payload }
    case 'clear_error_message':
      return { ...state, errorMessage: '' }
    default:
      state;
  }
}

const clearErrorMessage = dispatch => () => {
  dispatch({ type: 'clear_error_message' });
}

// ARROW FUNCTION EXAMPLE
const add = (a, b) => {
  return a + b;
}

const add2 = (a, b) => a + b;
// END EXAMPLE


const signup = dispatch => async ({ email, password }) => {
  try {
    const response = await trackApi.post('/signup', { email, password });
    await AsyncStorage.setItem('token', response.data.token);

    dispatch({ type: 'signin', payload: response.data.token })

    navigate('TrackList');

  } catch (err) {
    dispatch({
      type: 'add_error',
      payload: 'Something went wrong with sign up'
    });
  }
}


const signin = dispatch => async ({ email, password }) => {
  try {
    const response = await trackApi.post('/signin', { email, password });
    await AsyncStorage.setItem('token', response.data.token);

    dispatch({ type: 'signin', payload: response.data.token });

    navigate('TrackList');
  } catch (err) {
    dispatch({
      type: 'add_error',
      payload: 'Something went wrong with sign in'
    })
  }


}

const signout = dispatch => {
  return () => {

  }
}
export const { Provider, Context } = createDataContext(
  authReducer,
  { signup, signin, signout, clearErrorMessage },
  { token: null, errorMessage: '' }
)